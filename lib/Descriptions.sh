[ -n "$__DESCRIPTIONS" ] && return || readonly __DESCRIPTIONS=1

declare -a __Descriptions__stack=()
declare -i __Descriptions__stack_index=-1

function Descriptions__push() {
    __Descriptions__stack_index+=1
    __Descriptions__stack[$__Descriptions__stack_index]=""
}

function Descriptions__pop() {
    __Descriptions__stack_index+=-1
}

function Descriptions__get() {
    echo "${__Descriptions__stack[$__Descriptions__stack_index]}"
}

function Descriptions__set() {
    __Descriptions__stack[$__Descriptions__stack_index]="$1"
}
