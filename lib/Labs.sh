[ -n "$__LABS" ] && return || readonly __LABS=1

declare -a __Labs__map_from=()
declare -a __Labs__map_to=()

declare -a __Labs__shared_homes=()

function map() {
    local pattern="$1"
    shift    
    __Labs__map_from+=("$pattern")
    __Labs__map_to+=("$*")    
}

function shared_home() {
    local pattern="$1"
    __Labs__shared_homes+=("$pattern")
}


function Labs__read_conf() {
    source "$(dirname "$0")/labs.conf.sh"
}

function Labs__map_names() {
    # Translate a list of remote names according to the mapping rules in labs.conf.sh.
    for name in $*; do
        local -i index            
        local match=0
        
        # Iterate through the patterns.
        for (( index=0 ; index < ${#__Labs__map_from[@]} ; index++ )) ; do
            
            local pattern="^${__Labs__map_from[$index]}\$"
            
            # If a pattern matches, perform the mapping, and recurse (since the resulting name 
            # list might need further mapping).
            if [[ "$name" =~ $pattern ]]; then
                local new_name_sublist=""
                for subst in ${__Labs__map_to[$index]}; do
                    new_name_sublist+=" $(sed -r "s/$pattern/$subst/" <<<"$name")"
                done
                Labs__map_names "$new_name_sublist"
                match=1
                break
            fi
        done
        
        # None of the patterns matched, so we simply output this name unmodified.
        if [ "$match" -eq 0 ]; then
            echo $name
        fi
    done
}


function Labs__get_rsync_locations() {
    
    local -A patterns_done=()
    for name in $*; do
        local already_handled=0
        local -i index
        for (( index=0 ; index < ${#__Labs__shared_homes[@]} ; index++ )); do
            pattern="${__Labs__shared_homes[$index]}"
            
            if [[ "$name" =~ $pattern ]]; then
                if [ -z "${patterns_done[$index]}" ]; then
                    patterns_done[$index]=1                    
                else
                    already_handled=1
                fi
                break
            fi
        done
        
        if [ "$already_handled" -eq 0 ]; then
            echo $name
        fi
    done
}
