[ -n "$__LISTER" ] && return || readonly __LISTER=1

. "$(dirname "$0")/lib/Descriptions.sh"
. "$(dirname "$0")/lib/Selector.sh"
. "$(dirname "$0")/lib/Remotes.sh"
. "$(dirname "$0")/lib/Labs.sh"
. "$(dirname "$0")/lib/Display.sh"
. "$(dirname "$0")/lib/Util.sh"


function Lister__list()
{
    local test_spec="$1" unfiltered_remotes="$2" exclusions="$3"
    local some_tests=0
    
    Selector__include_all "$test_spec" || return 1
        
    for suite in $(Selector__get_test_suites); do    
        test_list="$(Selector__get_tests "$suite")"

        if [ -n "$test_list" ]; then
            # Show test suite description
            echo "$BRIGHT$(Selector__get_test_suite_description "$suite"):$NORMAL"
        
            for test_name in $test_list; do
                echo -e "  $suite.$test_name"
                some_tests=1
            done
        fi
    done
    
    if [ "$some_tests" -eq 0 ]; then
        echo "[No tests selected]"
    fi
    
    if [ -n "$remotes" ]; then
        echo
        echo "${BRIGHT}Machines to test:${NORMAL}"
        
        function __Lister__print_included() {
            echo "  $1"  
        }

        function __Lister__print_excluded() {
            echo "  $RED[excluded]$NORMAL $1"
        }
        __Remotes__filter "$unfiltered_remotes" "$exclusions" __Lister__print_included __Lister__print_excluded
        
        echo
        echo "${BRIGHT}Rsync locations:${NORMAL}"
        local remotes="$(__Remotes__filter "$unfiltered_remotes" "$exclusions")"
        local rsync_locations="$(Labs__get_rsync_locations $remotes)"
        for r in $rsync_locations; do
            echo "  $r"
        done
    fi
}
