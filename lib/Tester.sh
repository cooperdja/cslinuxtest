[ -n "$__TESTER" ] && return || readonly __TESTER=1

. "$(dirname "$0")/lib/Descriptions.sh"
. "$(dirname "$0")/lib/Selector.sh"
. "$(dirname "$0")/lib/Display.sh"
. "$(dirname "$0")/lib/Util.sh"

declare -a __Tester__messages=()
declare __Tester__pass
declare __Tester__warn
declare __Tester__fail_info
declare __Tester__error_level

declare __Tester__verbose
declare __Tester__debug


function get_file_type() {
    if [ -z "${1}" ]; then
        echo "Empty file type"
        return 1
    fi

    local first=1
    for (( i=0 ; i < ${#1} ; i+=1 )); do
    
        if [ $first == 0 ]; then
            echo -n ", "
        else
            first=0
        fi
    
        case "${1:$i:1}" in
            f) echo -n file ;;
            d) echo -n directory ;;
            l) echo -n symlink ;;
            L) echo -n recursive symlink ;;
            N) echo -n broken symlink ;;
            c) echo -n character device ;;
            b) echo -n block device ;;
            s) echo -n socket ;;
            p) echo -n fifo pipe ;;
            U) echo -n unknown file type;;
            *) 
                echo "Invalid file type: '${1:$i:1}'"
                return 1 
                ;;
        esac
    done
    echo
}

function description() {
    check $# 1 1
    Descriptions__set "$1"
}


# fail <message> [<message2>]
# ---
# Causes the current test to fail, while adding a message to the list of 
# messages to display. This DOES NOT actually abort the test, though (because
# there's no mechanism to do so from here in bash), so it could be called 
# multiple times on a single test, giving multiple messages.
function fail() {
    check $# 1 2
    case "$__Tester__error_level" in
        e) __Tester__pass=0 ;;
        w) __Tester__warn=1 ;;
    esac
    __Tester__messages+=("$RED$1$NORMAL")
    if [ -n "$2" ]; then
        __Tester__messages+=("$RED$2$NORMAL")
    fi
}


function assert_cmd() {
    local command="$1" terminal="" output_file="output.txt" output_pattern="" \
          accept_exit="" reject_exit="" min_version="" rm_pattern="" capture_pattern="" \
          fail_msg="" warning=""
    shift 1    
    
    # Validate keyword parameters
    for p in "$@"; do        
        if [[ ! "$p" =~ (terminal|output_file|output|accept_exit|reject_exit|min_version|rm_pattern|capture_pattern|fail_msg|warning)=.* ]]; then
            echo "assert_cmd: invalid parameter '$p'"
            return 1
        fi
    done
    
    # Assign keyword parameters to local variables
    if [ $# -ge 1 ]; then
        local "$@"
    fi
    
    # If we need to run the command in a fake terminal, we can wrap it as follows:
    local actual_command
    if [ -n "$terminal" ]; then
        run_command="script --quiet --flush --return --command ${command@Q} /dev/null"
    else
        run_command="$command"
    fi
    
    # Run the command and record the results
    bash -c "$run_command" &>"$output_file"
    local exit_status=$?
    if [ $__Tester__verbose -eq 1 ]; then
        # In verbose mode, we print the command and its output (as stored in <outputfile>).
        local output="$(<$output_file)"
        if [ -n "$output" ]; then
            __Tester__messages+=("Running '$command'..."$'\n'"$output")
        else
            __Tester__messages+=("Running '$command'...")
        fi
    fi
    
    # Distinguish errors and warnings
    if [ -n "$warning" ]; then
        __Tester__error_level="w"
    else
        __Tester__error_level="e"
    fi

    # Setup conditional defaults for accept_exit, based on whether reject_exit has been supplied.
    if [ -z "$accept_exit" ]; then
        if [ -z "$reject_exit" ]; then
            accept_exit="0"   # No accept/reject criteria -> accept only 0.
        else
            accept_exit=".*"  # Only reject criteria -> accept anything not rejected.
        fi
    fi
    
    # Check the exit status against the accept/reject exit status patterns.
    # (Note: exit_status inherently can't match an empty/default reject_exit.)
    if [[ ! $exit_status =~ ^$accept_exit$ || $exit_status =~ ^$reject_exit$ ]]; then
        case $exit_status in
            (126) fail "no execute permission ('$command')" "$fail_msg" ;;
            (127) fail "command not found ('$command')" "$fail_msg" ;;
            (*)   fail "'$command' returned an exit status of '$exit_status'" "$fail_msg" ;;
        esac
        return 1
    fi
    
    # Check the output against the acceptable output pattern.
    if [[ -n "$output" && ! "$(<$output_file)" =~ $output ]]; then
        fail "'$output' did not occur in the output of '$command'" "$fail_msg"
        return 1
    fi
    
    if [[ -n $min_version ]]; then
    
        local version_context
        
        # If an rm-pattern is specified, remove every match from the command output.
        if [ -n "$rm_pattern" ]; then
            version_context="$(sed --regexp-extended "s/$rm_pattern//g" <"$output_file")"
            
        # If a capture-pattern is specified, remove all *non*-matching output.
        elif [ -n "$capture_pattern" ]; then
            version_context="$(grep --extended-regexp --only-matching "$capture_pattern" <"$output_file")"
            
        else
            version_context="$(<$output_file)"
        fi
        
        # Look for a version number in the (remaining) command output.
        local version_pattern="[0-9]+([._p][0-9]+)*"
        local sed_script="s/^[^0-9]*($version_pattern).*$/\1/; t success; b; :success p; q;"
        local actual_version=$(sed --quiet --regexp-extended "$sed_script" <<<"$version_context")
        
        # Fail the test if we couldn't find a version number.
        if [[ ! "$actual_version" =~ ^$version_pattern$ ]]; then
            fail "Could not identify any version number in the output of '$command'" "$fail_msg"
            return 1
        fi
        
        # Set the actual version number to be displayed (whether the test passses or fails).
        __Tester__messages+=("${command% *} version $actual_version found (>=$min_version required)")    
        
        # Compare the minimum and actual version numbers component by component. 
        # At each iteration, extract the first number from each. If they are 
        # different, then the test passes or fails accordingly. If they are the
        # same, we remove them from the front of the *_suffix variables, and 
        # repeat.
        
        local min_suffix="$min_version."
        local actual_suffix="$actual_version."
        local actual_part
        
        while [ -n "$min_suffix" ]; do
            min_part=${min_suffix%%[._p]*}
            min_suffix=${min_suffix#*[._p]}
            
            if [ -n "$actual_suffix" ]; then
                actual_part=${actual_suffix%%[._p]*}
                actual_suffix=${actual_suffix#*[._p]}
                
            else
                actual_part=0
            fi
            
            if [ $actual_part -gt $min_part ]; then
                break
                
            elif [ $actual_part -lt $min_part ]; then
                fail "${command% *} version too old" "$fail_msg"
                return 1
            fi  
            # Otherwise, if they're equal, proceed to the next pair of version components. If this
            # was the last pair, then the actual version is exactly the required version.
        done
    fi
    
    return 0    
}

function assert_file() {
    local file="$1" type="" ltype="" user="" group="" permissions="" contents="" sha256="" fail_msg="" warning=""
    shift 1    
    
    # Validate keyword parameters
    for p in "$@"; do        
        if [[ ! "$p" =~ (type|ltype|user|group|permissions|contents|sha256|fail_msg|warning)=.* ]]; then
            echo "assert_file: invalid parameter '$p'"
            return 1
        fi
    done
    
    # Assign keyword parameters to local variables
    if [ $# -ge 1 ]; then
        local "$@"
    fi
    
    # Distinguish errors and warnings
    if [ -n "$warning" ]; then
        __Tester__error_level="w"
    else
        __Tester__error_level="e"
    fi
    
    # All important basic existence check.
    if [[ ! -e "$file" ]]; then
        fail "$file does not exist"
        return 1
    fi
    
    # Check the file type (file, directory, character device, etc.)
    local -i result=0
    if [ -n "$type" ]; then
        local actual_type="$(find "$file" -maxdepth 0 -printf %y)"
        if [[ ! "$actual_type" =~ ^[$type]$ ]]; then
            fail "$file is a $(get_file_type "$actual_type") (should be a $(get_file_type "$type"))." "$fail_msg"
            result=1
        fi
    fi
    
    # Check the file type, *but* this time automatically follow any symlinks.
    local -i result=0
    if [ -n "$ltype" ]; then
        local actual_ltype="$(find "$file" -maxdepth 0 -printf %Y)"
        if [[ ! "$actual_ltype" =~ ^[$ltype]$ ]]; then
              fail "$file is a $(get_file_type "$actual_ltype") (should be a $(get_file_type "$ltype"))."
          result=1
        fi
    fi
    
    if [ -n "$user" ]; then
        local actual_user="$(stat -c %U "$file")"
        if [[ ! "$actual_user" =~ ^$user$ ]]; then
            fail "$file is owned by user '$actual_user' (should match '$user')."
            result=1
        fi
    fi
    
    if [ -n "$group" ]; then
        local actual_group="$(stat -c %G "$file")"
        if [[ ! "$actual_group" =~ ^$group$ ]]; then
            fail "$file is owned by group '$actual_group' (should match '$group')."
            result=1
        fi
    fi
    
    if [ -n "$permissions" ]; then
        local actual_permissions="$(stat -c %a "$file")"
        if [[ ! "$actual_permissions" =~ ^$permissions$ ]]; then
            fail "$file has permissions '$actual_permissions' (should match '$permissions')."
            result=1
        fi
    fi
    
    if [ -n "$contents" ] && ! grep --quiet --extended-regexp "$contents" "$file" ; then
        # Note: we use 'grep' here rather than '=~' because it operates line-by-line (not all at 
        # once), allowing us to use '^' and '$' to match the beginning and end of each line.
        fail "$file does not contain '$contents'."
        result=1
    fi
    
    if [ -n "$sha256" ]; then
        local -a actual_sha256=( $(sha256sum "$file") )
        # Note: sha256sum prints the sum and then the filename. We only want the first.        
        if [ "${actual_sha256[0]}" != "$sha256" ]; then
            fail "$file is are out of date or otherwise incorrect; its SHA256 sum does not match '$sha256'."
            result=1
        fi
    fi
    
    if [[ $result -eq 1 && -n "$fail_msg" ]]; then
        fail "$fail_msg"
    fi
    
    return $result
}



# find_cmd <output-file> <command> [<command> [...]]
# ---
# Attempts to locate the specified <command>s. The test fails if none of the 
# commands can be found. Otherwise, the path to the first command to be found 
# is written to <output-file>.
function find_cmd() {
    check $# 2 1000
    
    local outputfile="$1"
    local commands=("${@:2}")
    local firstcommand="$2"
    
    choice="$(which "${commands[@]}" 2>/dev/null | head -1)"
    if [ -z "$choice" ]; then
        fail "Could not find any of $(Util__echo_list "${commands[@]}")"
        
        # Purely to make later (inevitable) error messages more meaningful, we 
        # arbitrarily pick the first command (even though it isn't there).
        echo "$firstcommand" >"$outputfile"
        return 1
        
    else
        __Tester__messages+=("$choice found (from among $(Util__echo_list "${commands[@]}"))")
        echo "$choice" >"$outputfile"
        return 0
    fi        
}


# fail_info <message>
# ---
# Provides a message to be displayed IF (a) the test has failed, and (b) a 
# previous message has not already been printed. This can be used to help 
# narrow down the cause of the failure and provide assistance to remedy it.
function fail_info() {
    check $# 1 1
    local info="$1"

    if [[ $__Tester__fail_info -eq 0 && $__Tester__pass -eq 0 ]]; then
        __Tester__messages+=("$BRIGHT$YELLOW$info$NORMAL")
        __Tester__fail_info=1
    fi
}


function Tester__run()
{
    local test_spec="$1" debug="$2" verbose="$3" remote_data="$4"
    local -i passed=0 warnings=0 failed=0
    local test_dir descr test_list
    
    __Tester__verbose="$verbose"
    __Tester__debug="$debug"
    
    Selector__include_all "$test_spec" || return 1
        
    for suite in $(Selector__get_test_suites); do
    
        test_list="$(Selector__get_tests "$suite")"

        if [ -n "$test_list" ]; then
            # Show test suite description
            if [ "$remote_data" -ne 1 ]; then
                echo "$BRIGHT$(Selector__get_test_suite_description "$suite"):$NORMAL"
            fi
        
            for test_name in $test_list; do
            
                # Setup test
                test_dir=$(mktemp -d ~/tmp.XXXXXX)
                __Tester__pass=1
                __Tester__warn=0
                __Tester__fail_info=0
                __Tester__error_level="e"
                __Tester__messages=()
                cd "$test_dir"
                
                # Run test
                Descriptions__push
                selected_test_${suite}_${test_name}
                descr="$(Descriptions__get)"
                Descriptions__pop

                # Tear down
                cd "$test_dir/.."
                if [[ $__Tester__debug -eq 1 && $__Tester__pass -eq 0 ]]; then
                    __Tester__messages+=("See $test_dir/")
                else
                    rm -rf "$test_dir"
                fi

                if [ "$remote_data" -eq 1 ]; then
                
                    # Simple format for transferring results back from a remote test
                    echo -e "$suite\t$test_name\t$__Tester__pass"
                    for message in "${__Tester__messages[@]}"; do
                        echo "+${message//$'\n'/$'\n'>}"
                    done  
                else
                    # Prettified format for local console output
                    if [ $__Tester__pass -eq 1 ]; then
                        if [ $__Tester__warn -eq 1 ]; then
                            warnings+=1
                            echo -en "  [$BRIGHT${YELLOW}warn$NORMAL]"
                        else
                            passed+=1
                            echo -en "  [$BRIGHT${GREEN}ok$NORMAL]"
                        fi
                        
                    else
                        failed+=1
                        echo -en "  [$BRIGHT${RED}FAIL$NORMAL]"
                    fi
                    echo -en "  $BRIGHT$suite.$test_name$NORMAL"
                    if [ -n "$descr" ]; then
                        echo -n " ($descr)"
                    fi
                    echo
                                    
                    if [[ $__Tester__verbose -eq 1 || $__Tester__pass -eq 0 || $__Tester__warn -eq 1 ]]; then
                        
                        for message in "${__Tester__messages[@]}"; do
                            Display__print_captured_output "    " "$message"
                        done
                        echo
                    fi
                fi
            done
            
            if [ "$remote_data" -ne 1 ]; then
                echo
            fi
        fi
    done

    if [ "$remote_data" -ne 1 ]; then
        echo "Total tests: $((passed + warnings + failed)), passed: $passed, warnings: $warnings, failed: $failed."
    fi
    
}
