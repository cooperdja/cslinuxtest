[ -n "$__SELECTOR" ] && return || readonly __SELECTOR=1

. "$(dirname "$0")/lib/Descriptions.sh"
. "$(dirname "$0")/lib/SuiteFunctions.sh"

declare -a __Selector__test_suite_list=()
declare -A __Selector__test_suite_set
declare -A __Selector__tests
declare -A __Selector__test_suite_descr

declare __Selector__ignore_include=0


function Selector__get_test_suites() {
    echo "${__Selector__test_suite_list[@]}"
}

function Selector__get_tests() {
    local suite="$1"
    echo "${__Selector__tests[$suite]}"
}

function Selector__get_test_suite_description() {
    local suite="$1"
    echo "${__Selector__test_suite_descr[$suite]}"
}

function __Selector__add_test() {
    local suite="$1" test_name="$2" test_function="$3"

    local mangled_name="selected_test_${suite}_${test_name}"
    
    # Create mangled function and add it to the list for this suite, 
    # but only if it isn't already there.
    if [ "$(type -t "$mangled_name")" != "function" ]; then
        eval "$(echo "${mangled_name}()"; typeset -f "$test_function" | tail -n +2)"
        __Selector__tests[$suite]="${__Selector__tests[$suite]} $test_name"
    fi
}


function Selector__include() {
    [ $__Selector__ignore_include -eq 1 ] && return
    
    local test_spec=$1 
    local suite test_name suite_file
    IFS="." read -r suite test_name <<<"$test_spec"
    local suitefile="$(dirname $0)/tests/$suite.tests.sh"
    
    if [ -r "$suitefile" ]; then
        Descriptions__push
        
        # Let the test suite define its tests...
        if [ -n "$test_name" ]; then
            # ... but DO NOT recursively process its includes, since we only 
            # want to include a single specific test.
            __Selector__ignore_include=1
            source "$suitefile"
            __Selector__ignore_include=0
        else
            # ... and DO recursively process includes, since we want to 
            # include everything.
            source "$suitefile"
        fi
        
        # Initialise test suite info (but only if this is the first time we've
        # seen it).
        if [ "${__Selector__test_suite_set[$suite]}" != 1 ]; then
            __Selector__test_suite_set[$suite]=1
            __Selector__test_suite_list+=("$suite")
            __Selector__tests[$suite]=""
            __Selector__test_suite_descr[$suite]="$(Descriptions__get)"
        fi
        
        if [ -n "$test_name" ]; then
            # Test name explicitly given; check it's valid.
            fn="test_$test_name"
            if [ "$(type -t "$fn")" == "function" ]; then
                __Selector__add_test "$suite" "$test_name" "$fn" 
            else
                echo "No such test: '$suite.$test_name'"
                return 1
            fi
            
        else
            # No test name given -- import all of them.
            for fn in $(compgen -A function test_); do
                __Selector__add_test "$suite" "${fn#test_}" "$fn"
            done
        fi
    
        # Delete the original test functions, to avoid mixups with the next
        # set of inclusions.
        for fn in $(compgen -A function test_); do
            unset -f "$fn"
        done
        Descriptions__pop
        return 0
        
    else
        echo "No such test suite: '$suite'"
        return 1
    fi
}


function Selector__include_all() {
    local test_spec="$1"
    
    for spec in $test_spec; do
        Selector__include "$spec" || return 1
    done
}
