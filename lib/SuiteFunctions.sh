[ -n "$__SUITE_FUNCTIONS" ] && return || readonly __SUITE_FUNCTIONS=1

. "$(dirname "$0")/lib/Util.sh"
. "$(dirname "$0")/lib/Descriptions.sh"
. "$(dirname "$0")/lib/Selector.sh"


function description() {
    check $# 1 1
    Descriptions__set "$1"
}

function include() {
    check $# 1 1
    Selector__include "$1"
}
