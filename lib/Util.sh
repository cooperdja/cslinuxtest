[ -n "$__UTIL" ] && return || readonly __UTIL=1

function Util__echo_list() {
    # Adapted from https://stackoverflow.com/questions/1527049/join-elements-of-an-array
    local IFS=", "
    echo -n "'$1"
    shift
    join="', '"
    printf "%s" "${@/#/$join}"
    echo -n \'
}

# Used internally to verify function parameters.
function check() {
    if [ "$1" -lt "$2" ] || [ "$1" -gt "$3" ]; then
        local callee=(`caller 0`) caller=(`caller 1`)
        echo -n "Invalid call to function ${callee[1]}() from ${caller[2]}:${caller[0]}. $1 parameter(s) given but "
        if [ $2 -eq $3 ]; then
            echo "$2 required."
        else
            echo "$2-$3 required."
        fi
        exit 1
    fi
}
