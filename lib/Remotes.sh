[ -n "$__REMOTES" ] && return || readonly __REMOTES=1

. "$(dirname "$0")/lib/Labs.sh"
. "$(dirname "$0")/lib/Descriptions.sh"
. "$(dirname "$0")/lib/Selector.sh"
. "$(dirname "$0")/lib/Display.sh"
. "$(dirname "$0")/lib/Util.sh"


function __Remotes__filter() {
    local unfiltered_remotes="$1" exclusions="$2" include_fn="${3:-echo}" exclude_fn="${4:-:}"

    Labs__read_conf
    
    local -A exclusion_set=()
    for ex in $(Labs__map_names "$exclusions"); do
        exclusion_set[$ex]=1
    done
    
    local -A remote_set=()
    local filtered_remotes=""
    for r in $(Labs__map_names "$unfiltered_remotes"); do
        if [ -z "${remote_set[$r]}" ]; then
            remote_set[$r]=1
            
            if [ -z "${exclusion_set[$r]}" ]; then
                $include_fn $r
                
            elif [ -n "$exclude_fn" ]; then
                $exclude_fn $r
            fi
        fi
    done
}


function __Remotes__wait_for() (
    local -r POLL_INTERVAL=1
    local -r DEFAULT_TEST_TIMEOUT=600
    local -r TEST_TIMEOUT_MULTIPLIER=10 # timeout = M * duration of the first test.
    
    local requiredRunning="$1"
    
    shopt -s nullglob
    local -a run_files=()
    
    # Count the number of simultaneously-running tests, and wait if 
    # necessary for that number to drop below a threshold. This is so we don't
    # schedule too many at once.
    
    local -i nRunning="$(run_files=(*.run) ; echo "${#run_files[@]}")"
    while [ "$nRunning" -gt "$requiredRunning" ]; do
        sleep $POLL_INTERVAL
        
        # While we're waiting (part 1): determine which tests have finished or failed to connect.
        local -a fin_files=(*.finished)
#         echo "Finished: \"${fin_files[@]}\""
        local now="$(date +%s)"
        for fin_file in "${fin_files[@]}"; do
            local remote="${fin_file%.finished}"
            if [ -f "$remote.error" ]; then
                echo "  -> $BRIGHT${RED}$remote: could not connect$NORMAL"
                Display__print_captured_output "    " "$(<$remote.error)"
                echo
            else
                echo "  -> $remote: finished"
                
                # For the first successfully-completed test, determine the duration, and
                # calculate a timeout value for other tests. (Note that we have to save this
                # value to a file to preserve it, since we're running in a sub-shell here.)
                if [ ! -s test_timeout.txt ]; then
                    local -i run_time=$((now - $(date +%s -r "$remote.pid") ))
                    echo $((run_time * TEST_TIMEOUT_MULTIPLIER)) >>test_timeout.txt
                fi
            fi
            rm -f "$fin_file" "$remote.run" "$remote.pid"
        done
        
        local -i test_timeout=DEFAULT_TEST_TIMEOUT
        if [ -s test_timeout.txt ]; then
            test_timeout=$(<test_timeout.txt)
        fi
        
        # While we're waiting (part 2): determine which tests have timed-out, and kill them.
        find -name '*.pid' -not -newermt "-${test_timeout} seconds" | \
            while read pid_file; do
                local basefile="$(basename "$pid_file")"
                local remote="${basefile%.pid}"
            
                # There is a slight possibility that the test will finish in the time it takes 
                # us to perform this check. If this happens, 'kill' will fail and the message will 
                # be superfluous.
                
                kill $(<$pid_file) 
                echo "  -> $BRIGHT${RED}$remote: timed out$NORMAL"
                if [[ -s "$remote.error" || -s "$remote.out" ]]; then
                    Display__print_captured_output "    " "stdout = "$'\n'"$(<$remote.out)"
                    Display__print_captured_output "    " "stderr = "$'\n'"$(<$remote.error)"
                fi
                echo
                rm -f "$pid_file" "$remote.run" "$remote.finished"
            done
        
#         echo Running: \"*.run\"
        nRunning="$(run_files=(*.run) ; echo "${#run_files[@]}")"
    done
)


function Remotes__run()
{
    local -r RSYNC_TIMEOUT=10
    local -r SSH_TIMEOUT=20
    local -r MAX_SIMULTANEOUS=20
    
    local unfiltered_remotes="$1" exclusions="$2" local_dir="$3" script_name="$4" test_spec="$5" debug="$6" verbose="$7"
    local remote_dir="~/$(basename "$local_dir")_remote"
    
    Labs__read_conf
    local remotes="$(__Remotes__filter "$unfiltered_remotes" "$exclusions")"
    
    Selector__include_all "$test_spec" || return 1
    
    local test_dir=$(mktemp -d ~/tmp.XXXXXX)
    cd "$test_dir"
    
    # Upload entire script directory tree to remote locations
    for rsync_loc in $(Labs__get_rsync_locations $remotes); do
        echo "Rsyncing with $rsync_loc."
        rsync \
            -e "ssh -o 'StrictHostKeyChecking no'" \
            --recursive \
            --exclude=".*" \
            --delete \
            --timeout="$RSYNC_TIMEOUT" \
            "$local_dir/" "$rsync_loc:$remote_dir/" \
            >&"rsync.error"
        if [ $? -ne 0 ]; then
            cat "rsync.error"
            return 1
        fi
    done
    echo
    
    local flags="--remote-data"
    [ "$debug" -eq 1 ]   && flags="$flags --debug"
    [ "$verbose" -eq 1 ] && flags="$flags --verbose"
    
    
    # Run remote tests
    for remote in $remotes; do
        __Remotes__wait_for $((MAX_SIMULTANEOUS - 1))
    
        echo "Starting tests on $remote."
        touch "$remote.run"
        (
            ssh -Y -o "StrictHostKeyChecking no" -o "ConnectTimeout $SSH_TIMEOUT" \
                "$remote" \
                "bash $remote_dir/$script_name $flags $test_spec" \
                >"$remote.out" 2>"$remote.error"
            if [ $? -eq 0 ]; then
                rm -f "$remote.error"
            fi
            touch "$remote.finished"
        ) &
        local pid=$!
        if [ -n "$pid" ]; then
            echo "$pid" >"$remote.pid"
        fi
    done

    __Remotes__wait_for 0
    echo

    local key
    local -A passed=()
    local -A failed=()
    local -A -i nPassed=()
    local -A -i nFailed=()
    local -i noResult
    local -i nRemotes=0
    
    for remote in $remotes; do
#         if [ -f "$remote.error" ]; then
#             echo "$BRIGHT${RED}Could not contact $remote:$NORMAL"
#             Display__print_captured_output "" "$(<$remote.error)"
#             echo
#             
#         else
        if ! [ -f "$remote.error" ]; then
            nRemotes+=1
            while read line; do
                if [[ "${line:0:1}" =~ [+\>] ]]; then
                    echo "$line" >>"$remote.$key"
                    
                else
                    IFS=$'\t' read suite test_name pass <<<"$line"                    
                    key="$suite.$test_name"
                    
                    # Note: if an associative integer array element is undefined, +=1 will set it to 1.
                    if [ "$pass" -eq 1 ]; then
                        passed[$key]+=" $remote"
                        nPassed[$key]+=1
                    else
                        failed[$key]+=" $remote"
                        nFailed[$key]+=1
                    fi
                fi
            done <"$remote.out"
        fi
    done
    
    for suite in $(Selector__get_test_suites); do
    
        test_list="$(Selector__get_tests "$suite")"

        if [ -n "$test_list" ]; then
            # Show test suite description
            if [ "$remote_data" -ne 1 ]; then
                echo "$BRIGHT$(Selector__get_test_suite_description "$suite"):$NORMAL"
            fi
        
            for test_name in $test_list; do
                key="$suite.$test_name"
                
                echo -n "  "
                
                if [ -n "${passed[$key]}" ]; then
                    echo -en "[$BRIGHT${GREEN}ok: ${nPassed[$key]}/$nRemotes$NORMAL] "
                fi
                    
                if [ -n "${failed[$key]}" ]; then
                    echo -en "[$BRIGHT${RED}FAIL: ${nFailed[$key]}/$nRemotes$NORMAL] "
                fi
                
                echo -e " $BRIGHT$suite.$test_name$NORMAL"

                local -A processed=()
                for remote in ${failed[$key]}; do
                
                    # Only output results if not already output (i.e. because a previous remote 
                    # already had identical results).
                    if [ -z "${processed[$remote]}" ]; then
                    
                        processed[$remote]=1
                        remote_msg="$(<$remote.$key)"
                    
                        # Find all remote machines that returned exactly the same test results as
                        # the current one.
                        remote_list="$BRIGHT$MAGENTA$remote$NORMAL"
                        for other_remote in ${failed[$key]}; do
                            if [ -z "${processed[$other_remote]}" ]; then
                                other_remote_msg="$(<$other_remote.$key)"
                                if [ "$remote_msg" == "$other_remote_msg" ]; then
                                    remote_list="$remote_list, $BRIGHT$MAGENTA$other_remote$NORMAL"
                                    processed[$other_remote]=1
                                fi
                            fi
                        done
                    
                        echo -e "    $remote_list:"
                        while read line; do
                            if [ "${line:0:1}" == "+" ]; then
                                echo "      $BRIGHT$YELLOW*$NORMAL ${line:1}"
                            else
                                echo "        > ${line:1}"
                            fi
                        done <"$remote.$key"
                        echo
                    fi
                done
            done
        fi
        
        echo        
    done                
    
    if [ $debug -eq 0 ]; then
        rm -rf "$test_dir"
    fi    
}
