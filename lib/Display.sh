[ -n "$__DISPLAY" ] && return || readonly __DISPLAY=1

declare NORMAL=$'\033[0m'
declare BRIGHT=$'\033[1m'
declare RED=$'\033[31m'
declare GREEN=$'\033[32m'
declare YELLOW=$'\033[33m'
declare MAGENTA=$'\033[35m'


function Display__print_captured_output() {
    local base_indent="$1" message="$2"
    echo -e "$base_indent$BRIGHT$YELLOW*$NORMAL ${message//$'\n'/$'\n'$base_indent  > }"
}
