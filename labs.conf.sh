# This file specifies a series of regex-based mappings, creating shortcuts 
# for lab machines and even groups of lab machines.

# Map names of the form '218a5' (and similar) to 'lab218-a05.cs.curtin.edu.au'
map '(lab)?2(18|19|20|21|32)-?([abcd])0?([1-6])' \
    'lab2\2-\30\4.cs.curtin.edu.au'
    
map '(saeshell|sae|ss|)0?([1234])p?' \
    'saeshell0\2p.curtin.edu.au'
    
# Map '218' to the entire set of machines in 314.218, and 218a (for instance)
# to the machines in row-A of lab 218.
map '218-?a' 218a{1,2,3,4,5}
map '218-?b' 218b{1,2,3,4}
map '218-?c' 218c{1,2,3,4}
map '218-?d' 218d{1,2,3,4,5}
map '218' 218{a,b,c,d}

map '219-?a' 219a{1,2,3,4,5,6}
map '219-?b' 219b{1,2,3,4,5,6}
map '219-?c' 219c{1,2,3,4,5,6}
map '219' 219{a,b,c}

map '220-?a' 220a{1,2,3,4,5,6}
map '220-?b' 220b{1,2,3,4,5,6}
map '220-?c' 220c{1,2,3,4,5,6}
map '220' 220{a,b,c}

map '221-?a' 221a{1,2,3,4,5}
map '221-?b' 221b{1,2,3,4}
map '221-?c' 221c{1,2,3,4}
map '221-?d' 221d{1,2,3,4,5}
map '221' 221{a,b,c,d}

map '232-?a' 232a{1,2,3,4}
map '232-?b' 232b{1,2,3,4,5}
map '232-?c' 232c{1,2,3,4,5}
map '232-?d' 232d{1,2,3,4,5}
map '232' 232{a,b,c,d}

map 'labs' 218 219 220 221 232
map '(saeshells?|sae|ss)' ss1 ss2 ss3 ss4
map 'all' labs ss

shared_home '(lab2(18|19|20|21|32)-[abcd]0[1-6]\.cs|saeshell0[1-4]p)\.curtin\.edu\.au'
