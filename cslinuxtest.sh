#!/bin/bash

# This is an automated test script for checking whether the lab image is 
# working (insofar as this can be tested automatically). There are several 
# generic checks, as well as unit-specific checks.

read -r -d '' splash_message <<EOF
Automated lab image test script.
+----------------------------------------------------------------------+
| WARNING: an '[ok]' is NOT a guarantee that something works,          |
| particularly for GUI-based or network-based programs. It only means  |
| that the item passed a simple command-line based check. There are    |
| many other things that might go wrong that cannot easily be detected |
| automatically. Thus, manual testing is always required!              |
+----------------------------------------------------------------------+
EOF

shopt -s compat41


function main() {
    local debug=0 list=0 remote_data=0 verbose=0 
    local parsing_remotes_now=0 parsing_exclusions_now=0
    local test_spec=""
    local remotes=""
    local exclusions=""

    local local_dir="$(readlink -f "$(dirname "$1")")"
    shift

    # Parse command-line args
    while [[ $# -gt 0 ]]; do
        case $1 in
            -d|--debug)
                debug=1
                shift
                ;;
                
            -e|--exclude)
                parsing_exclusions_now=1
                parsing_remotes_now=0
                shift
                ;;
                
            -l|--list)
                list=1
                shift
                ;;
                
            -r|--remote)
                parsing_remotes_now=1
                parsing_exclusions_now=0
                shift
                ;;
                
            --remote-data)
                remote_data=1
                shift
                ;;
            
            -v|--verbose)
                verbose=1
                shift
                ;;
                
            -*)
                echo "Unknown flag $1"
                exit 1
                ;;
                
            *)
                if [ "$parsing_remotes_now" -eq 1 ]; then
                    remotes+=" $1"
                    
                elif [ "$parsing_exclusions_now" -eq 1 ]; then
                    exclusions+=" $1"
                    
                else
                    test_spec+=" $1"
                fi
                shift
        esac
    done
    
    if [ -z "$test_spec" ]; then
        # If no test suites are explicitly specified, we test everything.
        
        for suite_path in "$local_dir/tests/"*.tests.sh; do
            suite_file="$(basename "$suite_path")"            
            test_spec="$test_spec ${suite_file%.tests.sh}"
        done
    fi
    
    if [ "$list" -eq 1 ]; then
        . "$local_dir/lib/Lister.sh"
        Lister__list "$test_spec" "$remotes" "$exclusions"
    
    else
        
        if [ "$remote_data" -eq 0 ]; then
            echo "$splash_message"
            echo
        fi
        
        if [ -z "$remotes" ]; then
            # Run local tests
            . "$local_dir/lib/Tester.sh"
            Tester__run "$test_spec" "$debug" "$verbose" "$remote_data"
        else
            source "$local_dir/lib/Remotes.sh"
            Remotes__run "$remotes" "$exclusions" "$local_dir" "$(basename "$0")" "$test_spec" "$debug" "$verbose"
        fi
    fi
}

main "$0" "$@"
