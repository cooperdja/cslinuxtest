description "Generic console tools"

function test_ssh() {
    assert_cmd "ssh -V" min_version=6.6
    assert_cmd "sftp" accept_exit="0|1" output="sftp"
    assert_cmd "scp"  accept_exit="0|1" output="scp"
}

function test_rsync() {
    assert_cmd "rsync --version" min_version=3.0
}

function test_dos2unix() {
    description "dos2unix and unix2dos"
    assert_cmd "dos2unix -V" min_version=6.0 rm_pattern="dos2unix"
    assert_cmd "unix2dos -V" min_version=6.0 rm_pattern="unix2dos"
}

function test_screen() {
    description "Terminal-based virtual terminal"
    assert_cmd "screen --version" accept_exit="0|1" min_version=4
}

function test_pdf_tools() {
    description "Converting between ps, pdf, etc."
    assert_cmd "ps2epsi --help"               output="ps2epsi"
    assert_cmd "ps2pdf"    accept_exit="0|1"  output="ps2pdf"
    assert_cmd "ps2ps"     accept_exit="0|1"  output="ps2ps"
    assert_cmd "pdf2ps"    accept_exit="0|1"  output="pdf2ps"
    assert_cmd "pdftotext" accept_exit="0|99" output="pdftotext"
    assert_cmd "dvipdf"    accept_exit="0|1"  output="dvipdf"
    assert_cmd "ps2eps --version"             output="ps2eps"
}

function test_web_tools() {
    assert_cmd "lynx --version"   min_version=2.8
    assert_cmd "elinks --version" min_version=0.12
    assert_cmd "curl --version"   min_version=7.19                
    assert_cmd "wget --version"   min_version=1.12
}

function test_archive_tools() {
    assert_cmd "tar --version"  min_version=1.23
    assert_cmd "gzip --version" min_version=1.3
    assert_cmd "bzip2 --help"   min_version=1.0.5 rm_pattern="bzip2.*Version"
    assert_cmd "zip --version"  min_version=3.0   rm_pattern="Copyright \(c\) [0-9]{4}-[0-9]{4}"
}
