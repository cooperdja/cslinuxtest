description "Common dev tools"

# --- Source code test data ---

read -r -d '' java6_test <<EOF
    import java.util.ArrayDeque;
    public class Test6
    {
        public static void main(String[] args)
        {
            System.out.println("Success");
            System.out.println(new ArrayDeque());
            System.out.println("You have Java 6 or higher");
        }
    }
EOF

read -r -d '' java7_test <<EOF
    public class Test7
    {
        public static void main(String[] args)
        {
            switch(Test7.class.getName())
            {
                case "Test7":
                    System.out.println("Success");
                    System.out.println("You have Java 7 or higher");
            }
        }
    }
EOF

read -r -d '' java8_test <<EOF
    import java.util.function.Supplier;
    public class Test8
    {
        public static void main(String[] args)
        {
            Supplier s = () -> "Success";
            System.out.println(s.get());
            System.out.println("You have Java 8 or higher");
        }
    }
EOF

read -r -d '' csharp_test <<EOF
    public class Test
    {
        public static void Main()
        {
            System.Console.WriteLine("Hello world");
        }
    }
EOF

read -r -d '' c_test <<EOF
    #include <stdio.h>
    int main()
    {
        printf("Hello world\n");
        return 0;
    }
EOF

read -r -d '' cpp_test <<EOF
    #include <iostream>
    int main()
    {
        std::cout << "Hello world" << std::endl;
        return 0;
    }
EOF

python_test='print("Hello world")'
ruby_test='puts "Hello world"'
perl_test='print "Hello world\n"'
r_test='cat("Hello world\n")'




# --- The test cases! ---

function set_java_version() {
    module purge &>/dev/null || return
    module load "java/$1"
}

function test_java() {
    description "v8+"
    set_java_version "8"
    assert_cmd "java -version" min_version=1.8
    assert_cmd "javac -version" min_version=1.8
    echo "$java8_test" >Test8.java
    assert_cmd "javac Test8.java"
    assert_cmd "java Test8" output="Success"
}

function test_java7() {
    set_java_version "7"
    assert_cmd "java -version" min_version=1.7
    assert_cmd "javac -version" min_version=1.7
    echo "$java7_test" >Test7.java
    assert_cmd "javac Test7.java"
    assert_cmd "java Test7" output="Success"
    
    echo "$java8_test" >Test8.java
    assert_cmd "javac Test8.java" warning=1 reject_exit="0" fail_msg="Not a strict Java 7 compiler."
}

function test_java6() {
    set_java_version "6"
    assert_cmd "java -version" min_version=1.6
    assert_cmd "javac -version" min_version=1.6
    echo "$java6_test" >Test6.java
    assert_cmd "javac Test6.java"
    assert_cmd "java Test6" output="Success"
    
    echo "$java7_test" >Test7.java
    assert_cmd "javac Test7.java" warning=1 reject_exit="0" fail_msg="Not a strict Java 6 compiler."
}

function test_python() {
    description "v2 v3"
    assert_cmd "python --version"  min_version=2.7
    assert_cmd "python2 --version" min_version=2.7
    assert_cmd "python3 --version" min_version=3.4
    
    echo "$python_test" >test.py 
    assert_cmd "python2 test.py" output="Hello world"
    assert_cmd "python3 test.py" output="Hello world"
}

function test_ruby() {
    description "v2"
    assert_cmd "ruby --version" min_version=2.0
    echo "$ruby_test" >test.rb
    assert_cmd "ruby test.rb" output="Hello world"
}

function test_perl() {
    description "v5"
    assert_cmd "perl --version" min_version="5" rm_pattern='This is perl[^.]*v'
    echo "$perl_test" >test.pl
    assert_cmd "perl test.pl" output="Hello world" 
}

function test_mono() {
    description "C#"
    assert_cmd "mcs --version"  min_version=2.10
    assert_cmd "mono --version" min_version=2.10
    echo "$csharp_test" >"test.cs" 
    assert_cmd "mcs test.cs"
    assert_cmd "mono test.exe" output="Hello world"
}
    
function test_r() {
    description ">=v3"
    assert_cmd "R --version" min_version=3
    assert_cmd "R --slave <<<'$r_test'" output="Hello world" 
}

function c_compiler_test()
{
    local command="$1" filename="$2" test_code="$3" standards="$4"
    
    description "$command: $standards"
    assert_cmd "$command --version" min_version=0

    echo "$test_code" > "$filename" 
    for std in $standards; do
        assert_cmd "$command --std=$std $filename"
        assert_cmd "./a.out" output="Hello world"
    done
}

function test_gcc_c() {    
    c_compiler_test "gcc" "test.c" "$c_test" "c89 c99"
}

function test_gcc_cpp() {
    # I'd like c++14 too, but let's not demand that for now.
    c_compiler_test "g++" "test.cpp" "$cpp_test" "c++03 c++11"
}

function test_clang_c() {    
    c_compiler_test "clang" "test.c" "$c_test" "c89 c99"
}

function test_clang_cpp() {
    # As above, I'd like c++14 too, but let's not demand that for now.
    c_compiler_test "clang++" "test.cpp" "$cpp_test" "c++03 c++11"
}

function test_gfortran() {
    # Not precisely sure who or what needs Fortran, but here it is.
    assert_cmd "gfortran --version" output="GNU Fortran" min_version=4.8
}

function test_octave() {
    # This is really here for Scientific Computing I think.
    assert_cmd "octave --version" output="Octave" min_version=3.8
}

function test_yasm() {
    # Not precisely sure who or what needs Yasm either, but here it is.
    assert_cmd "yasm --version" min_version=1.2
}

function test_ant() {
    assert_cmd "ant -version" min_version=1.9
}

function test_make() {
    assert_cmd "make --version" min_version=3.80
}

function test_valgrind() {
    assert_cmd "valgrind --version" min_version=3.8.1
}

function test_gdb() {
    assert_cmd "gdb --version" min_version=7.2
}

function test_cmake() {
    assert_cmd "cmake --version" min_version=2.8
}

function test_git() {
    assert_cmd "git --version" min_version=1.8
}

function test_mercurial() {
    assert_cmd "hg --version" min_version=2.6
}

function test_subversion() {
    assert_cmd "svn --version" min_version=1.6
}
