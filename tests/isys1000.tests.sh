description "ISYS1000 Database Systems (semester 2)"

function test_mysql()
{
    assert_cmd "mysql --version" min_version=15.1
}

# We can't run the MySQL server itself due to lack of permission.
