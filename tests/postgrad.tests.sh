description "Honours and postgraduate tools"


read -r -d '' latex_test <<EOF
    \documentclass{article}
    \message{Hello world}
    \begin{document}
        Test
    \end{document}
EOF



function test_latex() {
    description "TeX Live >= 2014"
    assert_cmd "latex --version" min_version=2014 capture_pattern="TeX Live 2[0-9]{3}"
    
    echo "$latex_test" >test.tex
    
    assert_cmd "latex test.tex" output="Hello world"
    assert_file test.dvi type=f
    
    assert_cmd "pdflatex test.tex" output="Hello world" 
    assert_file test.pdf type=f
    
    rm -f test.pdf test.dvi
    
    assert_cmd "xelatex test.tex" output="Hello world"
    assert_file test.pdf type=f
}

# Tex studio? No, we abandoned that one.
