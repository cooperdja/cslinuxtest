description "CNCO2000 Computer Communications (semester 1)"

include dev.gcc_c

read -r -d '' cnet_topology_test <<EOF
    compile = "test.c"
    
    host perth 
    {
        link to melbourne
    }
    
    host melbourne 
    {
        link to perth
    }       
EOF

read -r -d '' cnet_c_test <<EOF
    #include <cnet.h>
    void reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data)
    {
        fprintf(stderr, "Hello world");
    }
EOF



function test_cnet() {
    description "v3.3"
    assert_cmd "cnet" accept_exit="0|1" min_version=3.3
    echo "$cnet_topology_test" >TOPOLOGY
    echo "$cnet_c_test" >test.c
    assert_cmd "cnet -W -e 1s TOPOLOGY" output="Hello world"
}
