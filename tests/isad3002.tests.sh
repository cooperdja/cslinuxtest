description "ISAD3002 Software Metrics (semester 1)"

include dev.java

function test_javap() {
    description "Java disassembler"
    assert_cmd "javap -version" min_version=1.7
}

function test_hexdump() {
    assert_cmd "hexdump <<<'Hello world'" output="0000000 6548 6c6c 206f 6f77 6c72 0a64"
}
    
function test_pmd() {
    description "pmd.github.io"
    assert_cmd "pmd --help" accept_exit="0|1" output="pmd" 
}

function test_javacc() {
    description "Java Compiler Compiler"
    find_cmd javacc.txt javacc javacc.sh
    assert_cmd "$(<javacc.txt)" accept_exit="0|1" min_version=5.0
}
