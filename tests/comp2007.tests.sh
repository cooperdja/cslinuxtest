description "COMP2007 Programming Languages (semester 2)"

function test_gprolog() {
    # Any Prolog would have been suitable. Previously we had SWI-Prolog.
    description "GNU Prolog"
    assert_cmd "gprolog --version" min_version=1.4
}

function test_clojure() {
    # Any LISP/Scheme would have been suitable (e.g. GNU Common Lisp or MIT Scheme). Previously we had Racket.
    description "Clojure (a dialect of Lisp targetting the JVM)"
    assert_cmd "clojure -h" output="clojure"
}

function test_lex() {
    description "Lex"
    assert_cmd "lex --version" min_version=2.5
}

function test_yacc() {
    description "Yacc"
    assert_cmd "yacc -V" min_version=1.9
}

