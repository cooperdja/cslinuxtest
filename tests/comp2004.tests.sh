description "COMP2004 Computer Graphics (semester 2)"

include dev.gcc_c

function test_povray() {
    cat >povraytest.pov <<"    EOF"
        #include "colors.inc"
        #include "shapes.inc"
        #include "textures.inc"

        camera {
            location <0,2,-3>
            up <0,1,0>
            right <1,0,0>
            look_at <0,1,2>
        }
    EOF
    
    assert_cmd "povray povraytest.pov Display=false"
    fail_info "Likely POV-Ray is not installed. On Red Hat, you may need to bypass the package manager and install it in /usr/local.    See http://www.povray.org/."
    
    assert_cmd "povray --version" min_version="3.7" rm_pattern='cannot open the user configuration.*$' 
}

function test_imagemagick() {
    assert_cmd "convert --version" output="ImageMagick" min_version=6.7
}

function test_glut() {
    cat >gluttest.c <<"    EOF"
        #include <stdio.h>
        #include <GL/glut.h>
        
        int main(int argc, char** argv)
        {
            glutInit(&argc, argv);
            printf("Success");
            return 0;
        }
    EOF
    
    assert_cmd "gcc -lglut -lGLU -lGL gluttest.c -o gluttest"
    fail_info "Likely GLUT (or its 'devel' package) is not installed. On Red Hat, you may need to install 'freeglut-devel'."
    
    assert_cmd "./gluttest" output="Success"
    fail_info "Test code compiled but failed to run. Investigation needed."
}
