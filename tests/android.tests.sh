description "Android Studio/SDK"

include dev.java
include system.kvm

function test_kvm_access() {
    
    if ! [ -c /dev/kvm ]; then
        fail "Character device /dev/kvm is missing."
        exit 1
    fi
    
    stat -c %a /dev/kvm >perm.txt
    stat -c %G /dev/kvm >group.txt
        
    if grep -qE '.?..[67]' perm.txt; then

        # The device is world-readable/writable, so no need for a kvm group.
        true
        
    else
        assert_file /dev/kvm permissions=".?.[67]." group="kvm"
            
        assert_file /etc/group contents="^kvm:"
        assert_file /etc/group contents="^libvirt:"
        fail_info "The 'kvm' and/or 'libvirt' groups do not exist. Likely this means that you need to install qemu-kvm and/or libvirt."
        
        assert_file /etc/group contents="^kvm:[^:]*:[^:]*:([^:,]+){3}"
        assert_file /etc/group contents="^libvirt:[^:]*:[^:]*:([^:,]+){3}"
        fail_info "Nobody (or almost nobody) is in the 'kvm' and/or 'libvirt' groups. They probably need to be added."        
    fi
}

function test_local_dir() {
    assert_cmd "/local/android/sdk/emulator/emulator -list-avds"
    fail_info "Android SDK is missing or non-functional."
    
    assert_file /local/android/sdk    ltype=d permissions=".7[57][57]"
    assert_file /local/android/studio ltype=f permissions=".?[57][57]" \
        sha256="a6d0f4b9d1633130ed269c827c0a10f65d8bcf2b1b4742d90a1fe6de78d0984e"
    
    assert_file /local/android        ltype=d permissions="[137]777"
    fail_info "/local/android needs to be a world-writable directory with the sticky bit set."
}

function test_ide() {
    find_cmd androidstudio.txt /{usr/local,opt}/android-studio/bin/studio.sh
    assert_file "$(<androidstudio.txt)" permissions=".?..5"
    fail_info "$(<androidstudio.txt) is missing or has inadequate permissions."
    
    # We would normally consider just running the executable as follows (rather than stating it),
    # but it seems to randomly produce different error codes when run like this.
    #
    # assert_cmd "$(<androidstudio.txt) format"
}
