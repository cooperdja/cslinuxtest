description "System characteristics"

function test_64bit() {
    assert_cmd "uname -m" output="x86_64"
}

function test_kvm() {
    assert_file /proc/cpuinfo contents="vmx|svm"
    fail_info "Missing virtualisation hardware support. You may need to edit BIOS settings to enable either 'Intel VT-x' or 'AMD-V'."

    assert_file /dev/kvm type=c
    fail_info "Character device /dev/kvm is missing. If hardware support is present, this probably means the kernel was compiled without (for instance) the CONFIG_KVM option."
}

function test_unitsdir() {
    description "/usr/units"
    assert_file /usr/units ltype=d
    assert_file /usr/units/oopd type=d
    fail_info "This could indicate a network error."
}
