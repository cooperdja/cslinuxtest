description "COMP2003 Object Oriented Software Engineering (semester 1)"

include dev.ant
include dev.java
include dev.gcc_cpp
include dev.python


function test_pyqt5() {
    echo "import PyQt5 ; print('Success')" >test.py
    assert_cmd "python3 test.py" output="Success"
}

# We will disable this for now, since it appears both difficult to get working and relatively unimportant.
#     function test_python_gtk30() {
#         echo "import gi ;" \
#             "gi.require_version('Gtk', '3.0') ;" \
#             "from gi.repository import Gtk ;" \
#             "print('Success')" >test.py
# 
#         assert_cmd "python3 test.py" output="Success"
#     }

function test_gtkmm30() {
    cat >test.cpp <<"    EOF"
        #include <iostream>
        #include <gtkmm/main.h>
        int main(int argc, char** argv)
        {
            Gtk::Main kit(argc, argv);
            std::cout << "Success" << std::endl;
            return 0;
        }
    EOF
    
    assert_cmd "pkg-config --libs --cflags gtkmm-3.0" output_file="flags.txt"
    assert_cmd "g++ test.cpp -o gtkmmtest -std=c++11 $(<flags.txt)"
    fail_info "Likely Gtkmm 3.0 (and specifically its 'devel' package) is not installed properly. On Red Hat, you need to install 'gtkmm30-devel'."
    
    assert_cmd "./gtkmmtest" output="Success"
    fail_info "Test code compiled but failed to run. Note that this test needs X11 access."
}

function test_qt5() {
    cat >test.cpp <<"    EOF"
        #include <iostream>
        #include <QApplication>
        int main(int argc, char** argv)
        {
            std::cout << QApplication::tr("Hello").toStdString() << std::endl;
            std::cout << "Success" << std::endl;
            return 0;
        }
    EOF
    
    cat >test.pro <<"    EOF"
        TARGET = qttest
        SOURCES += test.cpp
        QT += widgets
    EOF
    
    find_cmd qmake-cmd.txt qmake-qt5 qmake
    assert_cmd "$(<qmake-cmd.txt) test.pro"
    fail_info "Likely Qt 5 (and specifically its 'devel' package) is not installed or configured properly. On Red Hat, you need to install 'qt5-qtbase-devel'."
    
    assert_cmd "make"
    fail_info "Qmake worked but the example code didn't compile. Investigation needed."
    
    assert_cmd "./qttest" output="Success"
    fail_info "Test code compiled but failed to run. Investigation needed."
}

#         import javafx.application.Application;
#         import javafx.stage.Stage;
# 
#         public class Test extends Application
#         {
#             public static void main(String[] args)
#             {
#                 Application.launch(args);
#             }
#             
#             @Override
#             public void start(Stage stage)
#             {
#                 System.out.println("Success");
#                 System.exit(0);
#             }
#         }        
#     EOF


function test_javafx() {
    cat >Test.java <<"    EOF"
        import javafx.application.Application;
        public class Test 
        {
            public static void main(String[] args)
            {
                try
                {
                    Application.launch(TestApp.class, args);
                }
                catch(UnsupportedOperationException e)
                {
                    // Happens if no X server is available
                    // Assume this is still okay.
                    System.out.println("Success (without X11)");
                }
            }
        }
    EOF
    
    cat >TestApp.java <<"    EOF"
        import javafx.application.Application;
        import javafx.stage.Stage;
        public class TestApp extends Application
        {
            @Override 
            public void start(Stage stage)
            {
                System.out.println("Success");
                System.exit(0);
            }
        }
    EOF

    # Set Java version to 8
    module purge &>/dev/null 
    module load "java/8" &>/dev/null
    
    assert_cmd "javac Test.java TestApp.java"
    fail_info "Likely JavaFX is not installed, OR not installed for the current version of Java ($(javac -version |& head -1), $(java -version |& head -1)). If not installed at all, then on Red Hat you need to install 'java-1.8.0-oracle-javafx'. However, you may also need to consult https://access.redhat.com/solutions/732883."
    
    assert_cmd "java Test" output="Success"
    fail_info "Test code compiled but failed to run. Investigation needed."
}

