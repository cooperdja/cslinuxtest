description "Desktop apps"

function test_firefox() {
    # nb. Firefox may output a "Glib-CRITICAL" warning message with several numbers; hence the deletion pattern below.
    assert_cmd "firefox --version" output="Firefox" min_version=38.6.1 rm_pattern='[^0-9.][0-9]+[^0-9.]'
}

function test_chrome() {
    find_cmd chrome.txt chrome google-chrome google-chrome-stable chromium
    assert_cmd "$(<chrome.txt) --version" output="Chrome"
}

function test_libreoffice() {
    find_cmd writer.txt lowriter oowriter
    find_cmd calc.txt localc oocalc

    assert_cmd "$(<writer.txt) --version" output="LibreOffice"
    assert_cmd "$(<calc.txt) --version" output="LibreOffice"
}

function test_gimp() {
    assert_cmd "gimp --version" min_version=2.8
}

function test_dia() {
    assert_cmd "dia --version" output="Dia"
}

function test_inkscape() {
    assert_cmd "inkscape --version" min_version=0.48
}

function test_okular() {
    assert_cmd "okular --version" min_version=0.16 rm_pattern='^[^O][^k][^u].*$'
}

function test_evince() {
    assert_cmd "evince --version" output="(Evince|GNOME Document Viewer)"
}

function test_filezilla() {
    assert_cmd "filezilla --version" accept_exit="0|255" min_version=3.7
}

function test_vmplayer() {
    description "VMware Player"
    assert_cmd "vmplayer --version" min_version=7.1.2
}

function test_virtualbox() {
    description "Oracle VirtualBox"
    assert_cmd "virtualbox --help" min_version=5.0 rm_pattern='kernel.*'
}
