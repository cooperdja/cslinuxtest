description "ISAD1000 Introduction to Software Engineering (semester 1)"

include dev.java
include dev.git

function test_junit() {
    cat >TestSuite.java <<"    EOF"
        import org.junit.*;
        import org.junit.runner.RunWith;
        import org.junit.runners.JUnit4;
        import static org.junit.Assert.*;

        @RunWith(JUnit4.class)
        public class TestSuite
        {
            @Test 
            public void test()
            {
                System.out.println("Success");
            }
        }
    EOF
    
    local cp="$CLASSPATH:/usr/units/ise/*" 
    
    assert_cmd "javac TestSuite.java"
    assert_cmd "java -cp $cp org.junit.runner.JUnitCore TestSuite" output="Success"
}

function test_mockito() {
    cat >TestSuite.java <<"    EOF"
        import static org.mockito.Mockito.*;
        
        public class TestSuite
        {
            public static void main(String[] args)
            {
                TestSuite mockTs = mock(TestSuite.class);
                System.out.println(mockTs.toString());
                System.out.println("Success");
            }
        }
    EOF
    
    local cp="$CLASSPATH:/usr/units/ise/*" 
    assert_cmd "javac TestSuite.java"
    assert_cmd "java -cp $cp TestSuite" output="Success"
}
