description "COMP3003 Software Engineering Concepts (semester 2)"

include dev.java
include dev.gcc_c
include android

function test_gradle() {
    find_cmd gradle.txt gradle /usr/local/gradle*/bin/gradle
    assert_cmd "$(<gradle.txt) --version" min_version=2.11
}

function test_maven() {
    # We have to delete Maven's control characters in order for assert_cmd to recognise the
    # output version number.
    
    assert_cmd "mvn --version" min_version=3.0 rm_pattern=$'\033'"\[[^m]*m"
}
