description "Editors and IDEs"

function test_vi() {
    assert_cmd "vi --version"  min_version=7.4
    assert_cmd "vim --version" min_version=7.4
}

function test_emacs() {
    assert_cmd "emacs --version" min_version=23
}

function test_nano() {
    assert_cmd "nano --version" min_version=2
}

function test_gvim() {
    # We need 'terminal=1', because apparently gvim doesn't display anything 
    # if it detects stderr being redirected.
    assert_cmd "gvim --version" terminal=1 output="Vi IMproved" 
}

function test_gedit() {
    assert_cmd "gedit --version" output="[gG]edit"
}

function test_kate() {
    description "KDE Advanced Text Editor"
    assert_cmd "kate --version" output="[kK]ate"
}

function test_netbeans() {
    find_cmd netbeans.txt netbeans /usr/bin/netbeans* /{usr/local,opt}/netbeans*/bin/netbeans*
    assert_cmd "$(<netbeans.txt) --nosplash --help" accept_exit="0|1|2" output="netbeans" 
}

function test_eclipse() {
    find_cmd eclipse.txt eclipse /usr/bin/eclipse* /{usr/local,opt}/eclipse*/eclipse*
    
    # I don't know how to run eclipse in just a 'help' or 'version' capacity.
    assert_file "$(<eclipse.txt)" ltype=f permissions=".?.[57][57]"
}

# Mono IDE?
